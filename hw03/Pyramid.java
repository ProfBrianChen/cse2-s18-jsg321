// Seth Greer
// 2/12/18
// Section 210
// The purpose of this program is to find the volume inside a pyramid
//
import java.util.Scanner;
//imports scanner class
public class Pyramid{
  
  public static void main(String[] args){
    
    
    Scanner myScanner = new Scanner (System.in);
    System.out.print("The square side of the Pyramid is (input length): "); //user inputs length of pyramid
    int square = myScanner.nextInt(); // assigns the variable as an Int
    
    Scanner input = new Scanner (System.in);
    System.out.print("The height of the Pyramid is (input height): "); //user inputs height of pyramid
    int height = input.nextInt(); // assigns the variable as an Int
    
    int volume = (square * square * height) / 3; // volume of a pyramid
    System.out.print("The volume inside the Pyramid is: " +volume); //prints output volume
    
    
    System.out.println(" "); // creates another line to seperate answer with terminal
  }
 
}