// Seth Greer
// 2/12/18
// Section 210
// The purpose of this program is to calculate the average acres of land affected by rain
//
import java.util.Scanner;
//imports scanner class
public class Convert {
  
  public static void main(String[] args){
    
    
    Scanner myScanner = new Scanner (System.in);  //creates a scanner object and assigns a reference variable
    System.out.print("Enter the affected area in acres xxxxx.xx: "); // prints acres of land
    double acres = myScanner.nextDouble(); //assigns the variable as a double

    Scanner r = new Scanner (System.in);
    System.out.print("Enter the rainfall in the affected area:  ");
    double rainfall = r.nextDouble();
    
    double output = acres * rainfall;   //calculates the average rainful per acre
    double acreInch = 40550399.587131; //conversion of one acre inch to one cubic mile
    double answer = output / acreInch;
    System.out.print(+answer+ " cubic miles"); //prints output
    
    
    
    System.out.println("");
   
    
  }

}