// Seth Greer
// 2/6/18
// Section 210
// The purpose of this program is to calculate the sales tax of the items
//
public class Arithmetic {
  
  public static void main(String[] args){
    
    
    //Number of pairs of pants
    int numPants = 3;
    //Cost per pair of pants
    double pantsPrice = 34.98;

    //Number of sweatshirts
    int numShirts = 2;
    //Cost per shirt
    double shirtPrice = 24.99;

    //Number of belts
    int numBelts = 1;
    //cost per box of envelopes
    double beltCost = 33.99;

    //the tax rate
    double paSalesTax = 0.06;

    double totalCostofPants; 
    totalCostofPants = numPants*pantsPrice; //calculates total cost
    System.out.print("Total cost of pants: "+totalCostofPants); //prints total cost
    System.out.println("");

    double totalCostofShirts; 
    totalCostofShirts = numShirts*shirtPrice; //calculates total cost
    System.out.print("Total cost of shirts: "+totalCostofShirts);  //prints total cost
    System.out.println("");

    double totalCostofBelts;
    totalCostofBelts = numBelts*beltCost; //calculates total cost
    System.out.print("Total cost of belts: "+totalCostofBelts); //prints total cost
    System.out.println("");
    
    double salesTaxonPants;
    salesTaxonPants = numPants*paSalesTax;
    System.out.print("Sales tax on Pants: "+salesTaxonPants);
    System.out.println("");
    
    double salesTaxonShirts;
    salesTaxonShirts = numShirts*paSalesTax;
    System.out.println("Sales tax on Shirts: "+salesTaxonShirts);
    System.out.println("");
    
    double salesTaxonBelts;
    salesTaxonBelts = numBelts*paSalesTax;
    System.out.println("Sales tax on Belts: "+salesTaxonBelts);
    System.out.println("");
    
    double totalCostofPurchases;
    totalCostofPurchases = totalCostofPants+totalCostofShirts+totalCostofBelts;
    System.out.println("Total cost of purchases: "+totalCostofPurchases);
    System.out.println("");
    
    double totalSalesTax;
    totalSalesTax = (numBelts+numShirts+numPants)*paSalesTax;
    System.out.println("Total sales tax: "+totalSalesTax);
    System.out.println("");
 
    double totalPaid;
    totalPaid = totalCostofPurchases+totalSalesTax;
    System.out.println("Total Paid: "+totalPaid);
    System.out.println("");
    
  }
}