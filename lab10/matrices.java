// Seth Greer
// 4/20/18
// Section 210
// The purpose of this program is to practice working with matrices
//
public class matrices{
  
  public static void main(String[] args){
  
    int[][] stuff = increasingMatrix(3, 3, true);
    printMatrix(stuff, true);
  }
  
  //so far you have row major
  public static int[][] increasingMatrix(int width, int height, boolean format){
    int[][] myArray = new int[width] [height];	
    int counter = 1;
    for (int i = 0; i < myArray.length; i++){
      for (int j = 0; j < myArray[i].length; j++){
        myArray[i][j] = counter;
        counter++;
        //myArray[0] = new int[3]; 
        // myArray[0][0] = 1; myArray[0][1] = 2; myArray[0][2] = 3;
        //  myArray[1] = new int[3]; 
        // myArray[1][0] = 4; myArray[1][1] = 5; myArray[1][2] = 6;
        //  myArray[2] = new int[3]; 
        // myArray[2][0] = 7; myArray[2][1] = 8; myArray[2][2] = 9;
      }
    }
     return myArray;
  }
  
  //so far you have row major
  public static void printMatrix(int[][] myArray, boolean format){
    for (int i = 0; i < myArray.length; i++){
        for (int j = 0; j < myArray[i].length; j++){
          System.out.print(myArray[i][j] + " ");
        }
      System.out.println();

    }  
  }
}