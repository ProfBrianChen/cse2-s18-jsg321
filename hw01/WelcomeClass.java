// Seth Greer
// 2/6/18
// Section 210 
// The purpose of this program is to print a welcome message
//
public class WelcomeClass {
  
  public static void main(String[] args){
    
    System.out.println("      -----------");
    System.out.println("     |  Welcome  |");
    System.out.println("      ____________ ");
    System.out.println("  ^   ^   ^   ^   ^   ^");
    System.out.println(" / \\ / \\ / \\ / \\ / \\ / \\");
    System.out.println("  <-J--S--G--3--2--1->");   
    System.out.println(" \\ / \\ / \\ / \\ / \\ / \\ /");
    System.out.println("  v   v   v   v   v   v");                   
    
  }
  
}