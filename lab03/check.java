// Seth Greer
// 2/9/18
// Section 210
// The purpose of this lab is to determine the cost of the check and how to calculate it
//
import java.util.Scanner;
// imports scanner classes
public class check{
  
  public static void main(String[] args){
    
    Scanner myScanner = new Scanner (System.in);
    System.out.print("Enter the original cost of the check in the form xx.xx: ");
    double checkCost = myScanner.nextDouble();
    
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): ");
    double tipPercent = myScanner.nextDouble();
    tipPercent /= 100; // gives user tip percentage in decimal value
    
    System.out.print("Enter the number of people who went out to dinner: ");
    int numPeople = myScanner.nextInt();
    
    double totalCost;
    double costPerPerson;
    int dollars, //whole dollar amount
              dimes, pennies; //these store digits
    totalCost = checkCost * (1 + tipPercent);
    costPerPerson = totalCost / numPeople;
    dollars = (int) costPerPerson;
    //gets whole amount
    // (int) (6.73 * 10) % 10 -> 67 % 10 -> 73
    // where the % (mod) operater returns the remainder
    // after the division: 583%100 -> 83, 27%5 -> 27
    dimes = (int) (costPerPerson * 10 ) % 10;
    pennies = (int) (costPerPerson * 100) %10;
    System.out.println("Each person in the group owes $" + dollars + '.' + dimes + pennies);
    
   
    
  }
  
}