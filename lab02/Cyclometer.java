// Seth Greer
// 2/2/18
// Section 210
// The purpose of the program is to record two kinds of data and output the numbers
//
public class Cyclometer {
  //main method
  public static void main(String[] args){
    int secsTrip1 = 480; //variable for seconds of trip 1
    int secsTrip2 = 32220; //variable for seconds of trip 2
    int countsTrip1 = 1561; //count for trip 1
    int countsTrip2 = 9037; //count for trip 2
    
    double wheelDiameter = 27.0; //tells the diameter of the wheel
    double PI = 3.1459; //variable for pi
    double feetPerMile = 5280; //tells the feet per mile
    double inchesPerFoot = 12; // tells the inches per foot
    double secondsPerMinute = 60; // tell the seconds per minute
    double distanceTrip1, distanceTrip2, totalDistance; //variables for the distances of each trip
    
    System.out.println("Trip 1 took "+
       	     (secsTrip1/secondsPerMinute)+" minutes and had "+
       	      countsTrip1+" counts."); //prints out the results of trip 2
	  System.out.println("Trip 2 took "+
       	     (secsTrip2/secondsPerMinute)+" minutes and had "+
       	      countsTrip2+" counts."); //prints out the results of trip 2

    distanceTrip1 = countsTrip1*wheelDiameter*PI;
    	// Above gives distance in inches
    	//(for each count, a rotation of the wheel travels
    	//the diameter in inches times PI)
  	distanceTrip1 /= inchesPerFoot * feetPerMile; // Gives distance in miles
	  distanceTrip2 = countsTrip2 * wheelDiameter * PI / inchesPerFoot / feetPerMile;
	  totalDistance = distanceTrip1 + distanceTrip2;
    
   
    System.out.println("Trip 1 was "+distanceTrip1+" miles"); // prints output of distances
	  System.out.println("Trip 2 was "+distanceTrip2+" miles");
	  System.out.println("The total distance was "+totalDistance+" miles");


  } //end of main method
 
}// end of class