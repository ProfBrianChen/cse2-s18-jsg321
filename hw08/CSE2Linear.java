// Seth Greer
// 4/8/18
// Section 210
// The puprose of this program is to practice with arrays and searching arrays
//
import java.util.Random;

import java.util.Scanner;

public class CSE2Linear{
 
  public static void main(String [] args) {
   
    Scanner myScanner = new Scanner(System.in);
    
    int [] grades = new int[15]; 
    
    System.out.println("Enter 15 integers for final grades:");
    
    int firstIntput = 0;
    int secIntput = 0;
    
    for (int i = 0; i < grades.length; i++){
      while (true){
        if (myScanner.hasNextInt()){
          firstIntput = myScanner.nextInt();
          if (firstIntput <= 100 || firstIntput >= 0){
            if(firstIntput >= secIntput){
              grades[i] = firstIntput;
              secIntput = firstIntput;
              
              break; 
            }
              
              else {
                System.out.println("Error: Enter correct values: ");
              }
          }
            
            
            else {
              System.out.println ("Error: Enter values in range: ");
            }
          }
        
          else {
            myScanner.next();
            System.out.println ("Error: Enter a correct integer: ");
          }
        
      }
        
    
  }
  
    
    int[] newArray;
    newArray= scrambledArray(grades);
    System.out.println("Scrambled: ");
    for (int i = 0; i<newArray.length; i++){
      System.out.println(newArray[i]+ " ");
    }
    
    System.out.println("Enter a grade to find: ");
    
    int rsearch = myScanner.nextInt();
    int linSearch = linearSearch(grades, 15, rsearch);
    
    if(linSearch > 0){
      System.out.println(rsearch + " was found " + linSearch+ " iterations");
    }
    
    else {
      System.out.println(rsearch+ " was  not found " + -linSearch+ " iterations");
      
    }
  }
  
  public static int binarySearch (int data, int[] array){
    int low = 0;
    int middle = 0;
    int high = array.length-1;
    int count = 0; 
    
    while (high >= low){
      count++;
      middle = (high + low)/2;
      
      if (array[middle] == data){
        return count;
      }
      if (array[middle] < data){
        low = middle + 1;
      }
      if (array[middle] > data){
        high = middle -1;
      }
    }
    return -count;
    }
  public static int[] scrambledArray(int[] array){
    int x = array.length;
    int y = 0;
    int z = 0;
    Random random = new Random ();
    random.nextInt();
    
    
    for (int i = 0; i < x; i++){
      z = random.nextInt(x);
      y = array[z];
      array[z] = array[i];
      array[i] = y;
    }
    return array;
        
        
    }
  public static int linearSearch(int array[],  int r, int x){
    int counter = 0;
    
    for(int i = 0; i < r; i++){
      counter++;
      
      if (array[i] == x){
        return counter;
      }
    }
    return -counter;
  }
}