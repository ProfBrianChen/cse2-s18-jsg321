// Seth Greer
// 4/5/18
// Section 210
// the purpose of this program is to practice using methods

import java.util.Random;
import java.util.Scanner;

public class methods{
  
  public static void main(String[] args){
    printStatment();
    Scanner newScanner = new Scanner(System.in);
    System.out.println("Would you like another sentence?");
    String input = newScanner.next();
    String y = "yes";
    String n = "no";
    while(true){
    if(input == y){
    printStatment();
      break;
    }
    else if(input == n){
      
    }
    }
    
    
  }
  
  public static void adjectives(){
  Random randomGenerator = new Random();
  int randomInt = randomGenerator.nextInt(10);
  switch (randomInt){
    case 1: System.out.print(" brave ");
      break;
    case 2: System.out.print(" calm ");
      break;
    case 3: System.out.print(" delightful ");
      break;
    case 4: System.out.print(" eager " );
      break;
    case 5: System.out.print(" faithful ");
      break;
    case 6: System.out.print(" gentle ");
      break;
    case 7: System.out.print(" happy ");
      break;
    case 8: System.out.print(" big ");
      break;
    case 9: System.out.print(" normal ");
      break;
      
  }
  }
  public static void subject(){
  Random randomGenerator = new Random();
  int randomInt = randomGenerator.nextInt(10);
  switch (randomInt){
    case 1: System.out.print(" he ");
      break;
    case 2: System.out.print(" she ");
      break;
    case 3: System.out.print(" it ");
      break;
    case 4: System.out.print(" them " );
      break;
    case 5: System.out.print(" they ");
      break;
    case 6: System.out.print(" I ");
      break;
    case 7: System.out.print(" you ");
      break;
    case 8: System.out.print(" man ");
      break;
    case 9: System.out.print(" woman ");
      break;
      
  }
  }
  public static void verb(){
  Random randomGenerator = new Random();
  int randomInt = randomGenerator.nextInt(10);
  switch (randomInt){
    case 1: System.out.print(" ran ");
      break;
    case 2: System.out.print(" looked ");
      break;
    case 3: System.out.print(" called ");
      break;
    case 4: System.out.print(" felt " );
      break;
    case 5: System.out.print(" took ");
      break;
    case 6: System.out.print(" showed ");
      break;
    case 7: System.out.print(" passed ");
      break;
    case 8: System.out.print(" laughed ");
      break;
    case 9: System.out.print(" jumped ");
      break;
      
  }
  }
  public static void object(){
  Random randomGenerator = new Random();
  int randomInt = randomGenerator.nextInt(10);
  switch (randomInt){
    case 1: System.out.print(" flower ");
      break;
    case 2: System.out.print(" book ");
      break;
    case 3: System.out.print(" toy ");
      break;
    case 4: System.out.print(" house " );
      break;
    case 5: System.out.print(" car ");
      break;
    case 6: System.out.print(" puddle ");
      break;
    case 7: System.out.print(" bread ");
      break;
    case 8: System.out.print(" table ");
      break;
    case 9: System.out.print(" phone ");
      break;
      
  }
  }
  public static void printStatment(){
    System.out.print("The");
    adjectives();
    object();
    verb();
    System.out.print("the");
    verb();
    object();
    System.out.println();
  }
  
  
}