// Seth Greer
// 4/19/18
// Section
//
import java.util.Scanner;
public class passingArrays 
{
  public static void main(String[]args)
  {
    Scanner myScanner = new Scanner (System.in);
    
    int [] array0 = {0,1,2,3,4,5,6,7};
    int [] array1 = copy(array0);
    int [] array2 = copy(array0);
    
    inverter(array0);
    
    System.out.println ("Array 0 printing");
    
    print(array0);
    
    System.out.println();
    inverter2(array1);
    System.out.print("Array 1 printing");
    print(array1);
    
    System.out.println();
    
    int[] array3 = inverter2(array2);
    
    System.out.println("Array 3 printing");
    
    print(array3);
    
  }
  
  public static int[] copy(int[]array)
  {
    int[] rArray= new int[array.length];
    
    for(int i =0; i<array.length; i++)
    {
      rArray[i] = array[i];
    }
    return rArray;
  }
  
  public static void inverter(int[] array)
  {
    for(int i= 0; i<array.length/2; i++)
    {
      int temper= array[i];
      array [i] = array[array.length-1-i];
      
      array[array.length-1-i]= temper;
    }
  }
  
  public static int[] inverter2(int[] array){
    int[]a = copy(array);
    
    for(int i=0; i<a.length/2; i++)
    {
      int temper = a[i];
      
      a[i]= a [a.length-1-i];
      
      a [a.length-1-i]= temper;
      
    }
    return a;
  }
  
  public static void print(int[] array)
  {
    for (int i=0; i<array.length;i++)
    {
      System.out.print(array[i]+ " ");
    }
  }
}