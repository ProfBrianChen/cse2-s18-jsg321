// Seth Greer
// 4/23/18
// Section 210
// The purpose of this program is to practice with multidimensional arrays
//
import java.util.Random;
public class RobotCity{
  
  public static void main(String[] args){
    Random random = new Random();
    int ran = random.nextInt((5)) + 10;
    int[][] stuff = buildCity(ran, ran, true);
   for (int i = 0; i < 5; i++){
     cityArray(stuff, true);
    display(stuff, true);
    System.out.println();
    invade(stuff, true);
   }
    
  }
  
  //builds city
  public static int[][] buildCity(int width, int height, boolean format){
    int[][] myArray = new int[width] [height];	
    int counter = 1;
    for (int i = 0; i < myArray.length; i++){
      for (int j = 0; j < myArray[i].length; j++){
        //make rand 
        Random num = new Random();
        int block = num.nextInt((899) ) + 100;
        myArray[i][j] = block;
        counter++;
        
      }
    }
     return myArray;
  }
  
  //prints city
  public static void cityArray(int[][] myArray, boolean format){
    for (int i = 0; i < myArray.length; i++){
        for (int j = 0; j < myArray[i].length; j++){
          System.out.print(myArray[i][j] + " ");
        }
      System.out.println();

    }  
  }
  
  public static void display(int[][] myArray, boolean format){
     for (int i = 0; i < myArray.length; i++){
        for (int j = 0; j < myArray[i].length; j++){
    System.out.printf("%5d", myArray[i][j]);
        }
       System.out.println();
     }
  }
  
  public static void invade(int[][] myArray, boolean format){
    for (int i = 0; i < myArray.length; i++){
        for (int j = 0; j < myArray[i].length; j++){
          Random random2 = new Random();
         // int k = random2.nextInt((-1));
          System.out.printf("%5d", myArray[i][j] * -1);
        }
       System.out.println();
     }
    
  }
  
}
