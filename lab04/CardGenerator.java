// Seth Greer
// 2/16/18
// Section 210
// The purpose of this lab is to print out the correct cards picked by the user
//
public class CardGenerator {
  
  public static void main(String[] args){
    
    int randomNumber = (int)(Math.random() * 52) + 1;
    
    if (randomNumber == 1){
      System.out.println("You picked the " +randomNumber+ " of diamonds");
    }
    else if (randomNumber == 2){
      System.out.println("You picked the " +randomNumber+ " of diamonds");
    }
    else if (randomNumber == 3){
      System.out.println("You picked the " +randomNumber+ " of diamonds");
    }
    else if (randomNumber == 4){
      System.out.println("You picked the " +randomNumber+ " of diamonds");
    }
    else if (randomNumber == 5){
      System.out.println("You picked the " +randomNumber+ " of diamonds");
    }
    else if (randomNumber == 6){
      System.out.println("You picked the " +randomNumber+ " of diamonds");
    }
    else if (randomNumber == 7){
      System.out.println("You picked the " +randomNumber+ " of diamonds");
    }
    else if (randomNumber == 8){
      System.out.println("You picked the " +randomNumber+ " of diamonds");
    }
    else if (randomNumber == 9){
      System.out.println("You picked the " +randomNumber+ " of diamonds");
    }
    else if (randomNumber == 10){
      System.out.println("You picked the " +randomNumber+ " of diamonds");
    }
    else if (randomNumber == 11){
      System.out.println("You picked the " +randomNumber+ " of diamonds");
    }
    else if (randomNumber == 12){
      System.out.println("You picked the " +randomNumber+ " of diamonds");
    }
    else if (randomNumber == 13){
      System.out.println("You picked the " +randomNumber+ " of diamonds");
    }
    
   
    if (randomNumber == 14){
      System.out.println("You picked the ace of clubs");
    } 
    else if (randomNumber == 15){
      System.out.println("You picked the 2 of clubs");
    }
    else if (randomNumber == 16){
      System.out.println("You picked the " +randomNumber+ " of clubs");
    }
    else if (randomNumber == 17){
      System.out.println("You picked the " +randomNumber+ " of clubs");
    }
    else if (randomNumber == 18){
      System.out.println("You picked the " +randomNumber+ " of clubs");
    }
    else if (randomNumber == 19){
      System.out.println("You picked the " +randomNumber+ " of clubs");
    }
    else if (randomNumber == 20){
      System.out.println("You picked the " +randomNumber+ " of clubs");
    }
    else if (randomNumber == 21){
      System.out.println("You picked the " +randomNumber+ " of clubs");
    }
    else if (randomNumber == 22){
      System.out.println("You picked the " +randomNumber+ " of clubs");
    }
    else if (randomNumber == 23){
      System.out.println("You picked the " +randomNumber+ " of clubs");
    }
    else if (randomNumber == 24){
      System.out.println("You picked the " +randomNumber+ " of clubs");
    }
    else if (randomNumber == 25){
      System.out.println("You picked the " +randomNumber+ " of clubs");
    }
    else if (randomNumber == 26){
      System.out.println("You picked the king of clubs");
    }
    
    
    if (randomNumber == 27){
      System.out.println("You picked the " +randomNumber+ " of hearts");
    } 
    else if (randomNumber == 28){
      System.out.println("You picked the " +randomNumber+ " of hearts"); 
    }
    else if (randomNumber == 29){
      System.out.println("You picked the " +randomNumber+ " of hearts"); 
    }
    else if (randomNumber == 30){
      System.out.println("You picked the " +randomNumber+ " of hearts"); 
    }
    else if (randomNumber == 31){
      System.out.println("You picked the " +randomNumber+ " of hearts"); 
    }
    else if (randomNumber == 32){
      System.out.println("You picked the " +randomNumber+ " of hearts"); 
    }
    else if (randomNumber == 33){
      System.out.println("You picked the " +randomNumber+ " of hearts"); 
    }
    else if (randomNumber == 34){
      System.out.println("You picked the " +randomNumber+ " of hearts"); 
    }
    else if (randomNumber == 35){
      System.out.println("You picked the " +randomNumber+ " of hearts"); 
    }
    else if (randomNumber == 36){
      System.out.println("You picked the " +randomNumber+ " of hearts"); 
    }
    else if (randomNumber == 37){
      System.out.println("You picked the " +randomNumber+ " of hearts"); 
    }
    else if (randomNumber == 38){
      System.out.println("You picked the " +randomNumber+ " of hearts"); 
    }
    else if (randomNumber == 39){
      System.out.println("You picked the " +randomNumber+ " of hearts"); 
    }
    
    
    if (randomNumber == 40){
      System.out.println("You picked the " +randomNumber+ " of spades");
    }
    else if (randomNumber == 41){
     System.out.println("You picked the " +randomNumber+ " of spades");
    }
    else if (randomNumber == 42){
     System.out.println("You picked the " +randomNumber+ " of spades");
    }
    else if (randomNumber == 43){
     System.out.println("You picked the " +randomNumber+ " of spades");
    }
    else if (randomNumber == 44){
     System.out.println("You picked the " +randomNumber+ " of spades");
    }
    else if (randomNumber == 45){
     System.out.println("You picked the " +randomNumber+ " of spades");
    }
    else if (randomNumber == 46){
     System.out.println("You picked the " +randomNumber+ " of spades");
    }
    else if (randomNumber == 47){
     System.out.println("You picked the " +randomNumber+ " of spades");
    }
    else if (randomNumber == 48){
     System.out.println("You picked the " +randomNumber+ " of spades");
    }
    else if (randomNumber == 49){
     System.out.println("You picked the " +randomNumber+ " of spades");
    }
    else if (randomNumber == 50){
     System.out.println("You picked the " +randomNumber+ " of spades");
    }
    else if (randomNumber == 51){
     System.out.println("You picked the " +randomNumber+ " of spades");
    }
    else if (randomNumber == 52){
     System.out.println("You picked the " +randomNumber+ " of spades");
    }
    
    System.out.println();
  }
}
