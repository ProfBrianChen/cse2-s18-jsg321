// Seth Greer
// 4/16/18
// Section 210
// The purpose of this program is to practice manipulating arrays
//
import java.util.Scanner;

import java.util.Random;

public class DrawPoker{
  public static void main(String[]args){
    Scanner scan = new Scanner(System.in);
    int choose = 0;
   
    int deck [] = initialize();
    do{
      for(int i = 0; i <= 51; i++){
      }
      System.out.println();
      System.out.println("Shuffle: ");
      int shuffleDeck[] = shuffle(deck);
      for(int i = 0; i <= 51; i++) {

      }
      System.out.println();
      
      int[] p1   = new int[5];
      int[] p2 = new int[5];

      int x = 0;
      int z = 0;

      for(int i = 0; i < 10; i++) {
        if (i % 2 == 0){
          p1 [x] = shuffleDeck[i];
          x++;
        }
        else {
          p2 [z] = shuffleDeck[i];
          z++;
        }
      }

      String hand1 = printCards(p1);
      System.out.println("Player 1 Hand: " +hand1);
      String hand2 = printCards(p2);
      System.out.println("Player 2 Hand: " +hand2);
      
      System.out.println("Player 1:");
      if(pair(p1)) {
        System.out.println("Pair");
      }
      else if(threekind(p1)) {
        System.out.println("Three of a Kind");
      }
      else if(flush(p1)) {
        System.out.println("Flush");
      }
      else if(fullhouse(p1)) {
        System.out.println("Full House");
      }
      else {
        System.out.println("High Card");
      }
      
      System.out.println("Player 2:");
      if(pair(p2)) {
        System.out.println("Pair");
      }
      else if(threekind(p2)) {
        System.out.println("Three of a Kind");
      }
      else if(flush(p2)) {
        System.out.println("Flush");
      }
      else if(fullhouse(p2)) {
        System.out.println("Full House");
      }
      else {
        System.out.println("High Card");
      }
      
      
      System.out.println("Enter 1 to play again. Enter 0 to quit:");
      choose = scan.nextInt();
    } while(choose == 1);
    
   
    
  }
  
  public static boolean pair(int[] hand) {
    for (int i = 0; i < 5 ; i++) {
      hand [i] = hand[i] % 13;
    }
    for(int i = 0; i < 5; i++){
      for(int j = 0; j < 5; j++) {
        if(i == j){
          continue;
        }
        if(hand[i] == hand[j]) {
          if(threekind(hand) == true) { 
            return false;
          }
          return true; 
        }
      }
    }
    return false;
} 

  public static boolean threekind(int[] hand) {
    for (int i = 0; i < 5; i++) {
      hand[i] = hand[i] % 13; 
    }
    for(int i = 0; i < 5; i++){
      for(int j = 0; j < 5; j++) {
        for(int k = 0; k < 5; k++){
        if(i == j || j == k || i == k){ 
        continue;  
        }
        if(hand[i] == hand[j] & hand[i] == hand[k]) {
          return true;
        }
       }
      }
     }
    return false;
  }
  
  public static boolean flush(int[] hand) {
    int count1 = 0;
    int count2 = 0;
    int count3 = 0;
    int count4 = 0;
    
    for (int i = 0; i < 5; i++) {
      if (hand[i] <= 12){
        count1++;
      }
      else if(hand[i] > 12 & hand[i] <= 25){
        count2++;
      }
      else if(hand[i] > 25 & hand[i] <= 38){
        count3++;
      }
      else if(hand[i] > 38 & hand[i] <= 51){
        count4++;
      }
      else{
        return false;
      } 
    }
    if(count1 == 5 || count2 == 5|| count3 == 5|| count4 == 5) {
      return true;
    }
    return false;
  }
  
  public static boolean fullhouse(int[] hand) { 
    boolean threecount = false;
    boolean paircount = false;
    
    for (int i = 0; i < 5; i++) {
      hand[i] = hand[i] % 13;
    }
    
    int rank[] = new int[13]; 
    for (int i = 0; i < 13; i++) {
      rank[i] = 0;
    }
    for (int i = 0; i < 5; i++) {
      int number = hand[i];
      
      rank[number] = rank[number] + 1;
    }
    for(int i = 0; i < 13; i++) { 
      if(rank[i] == 2) { 
        paircount = true;
        break;
      }
    }
    for(int i = 0; i < 13; i++) { 
      if(rank[i] == 3) {
        threecount = true;
        break;
      }
    }
    
    if(threecount == true & paircount == true) {
      return true;
    }
   
    return false;
  }
  
  public static int[] initialize(){
    int deck[] = new int[52];
    for (int i = 0; i <= 51; i++){
      deck[i] = i;
    }
    return deck;
  }
  
  public static int[] shuffle(int deck[]){
    int temp = 0;
    for(int i = 0; i <= 51; i++) {
      int j = (int)(Math.random() * 52); 
      temp = deck[j]; 
      deck[j] = deck[i]; //swaps existing value with random value
      deck[i] = temp;
    }
    return deck;
  } 
  
  public static String printCards(int[] cards){
    String suit = "";
    String card = "";
    
    String hand = "";
  
    for (int i = 0; i < cards.length; i++) {
      
      if(cards[i] <= 12){
        suit = "Diamonds";
      }
      else if(cards[i] > 12 & cards[i] <= 25){
        suit = "Clubs";
      }
      else if(cards[i] > 24 & cards[i] <= 38){
        suit = "Hearts";
      }
      else {
        suit = "Spades";
      }
      
      
      int b = cards[i] % 13;
      if(b == 0) {
        card = "Ace";
      }
      else if(b == 12){
        card = "King";
      }
      else if (b == 11){
        card = "Queen";
      }
      else if(b == 10){
        card = "Jack";
      }
      else {
        card = b+ "";
      }
      hand += (card+ "-" +suit+ "  ");
      
      card = "";
      suit = "";
    }
    return hand;
  }

}